"use strict";
function end(currentPage) {
    return function () {
        var nextPage = document.getElementById("page" + (currentPage + 1));
        nextPage.style.zIndex = 1;
    }
}

function load() {
    var pages = document.getElementsByClassName("page");
    for (var i = 0; i < pages.length; i++){
        var p = pages[i];
        p.addEventListener("click",function () {
            this.classList.add("pageAnimation");
        })
        p.addEventListener("animationend", end(i));

    }
}
